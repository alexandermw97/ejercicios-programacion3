/* Ejercicio 2: Suma consecutiva */

#include <stdio.h>
#include <stdlib.h>

sumaTotal(int rango)
{
int suma=0;
int i;
for (i = 1 ; i<=rango ; i++)
    suma+=i;
return(suma);

}

main()
{
    int numero,resultado;
    printf("Ingrese el numero n: ");
    scanf("%d",&numero);

    if (numero<=50)
    {
        resultado=sumaTotal(numero);
        printf("La suma es %d\n",resultado);
    }

    else
    {
        printf("Numero mayor a 50, no se puede");
    }

system("pause");
return 0;
}

/* Ejercicio 7: Perimetro triangulos*/
#include <stdio.h>
#include<stdlib.h>

int main() {
    int operador;
    float perimetro,longitud1, longitud2,longitud3;

    printf("Perimetro de triangulos\n\n");
    printf("Que triangulo eliges?\n");
    printf("1.-Equilatero\n");
    printf("2.-Isosceles\n");
    printf("3.-Escaleno\n");
    scanf("%c", &operador);
    system("cls");
    switch(operador)
    {
        case '1':
            printf("Perimetro de un triangulo Equilatero\n");
            printf("\nDame la longitud: ");
            scanf("%f",&longitud1);
            perimetro=(longitud1*3);
            printf("\nEl perimetro del triangulo equilatero es: %.3f\n ",perimetro);
        break;

        case '2':
            printf("Perimetro de un triangulo Isosceles\n");
            printf("\nDame la longitud1: ");
            scanf("%f",&longitud1);
            printf("\nDame la longitud2: ");
            scanf("%f",&longitud2);
            perimetro=(longitud1*2)+longitud2;
            printf("\nEl perimetro del triangulo isosceles es: %.3f\n ",perimetro);
        break;

        case '3':
              printf("Perimetro de un triangulo Escaleno\n");
              printf("\nDame la longitud1: ");
              scanf("%f",&longitud1);
              printf("\nDame la longitud2: ");
              scanf("%f",&longitud2);
              printf("\nDame la longitud3: ");
              scanf("%f",&longitud3);
              perimetro=longitud1+longitud2+longitud3;
              printf("\nEl perimetro del triangulo escaleno es: %.3f\n ",perimetro);
        break;

        default:
            printf("Error!");
    }

    return 0;
}

/* Ejercicio 14: adivina el numero*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
Random(void)
{
    srand(time(NULL));
}
int main()
{
    Random();
    printf("******ADIVINA EL NUMERO****** \n\n");
    int numero, numeroAdivinado=0, numeroAdivinar=rand()%100+1, numeroOportunidades=0;
    while (!numeroAdivinado)
    {
        printf("Escribe un numero entre 1 y 100: ");
        scanf("%d", &numero);

        if(numero <=100)
        {
            if(numeroAdivinar==numero)
            {
                numeroAdivinado=1;
            }
            ++numeroOportunidades;
        }
        else
        {
            printf("\n\nDijimos del 1 al 100\n\n");
        }

        if(numeroOportunidades >= 5)
        {
            printf("PERDISTE !\n\n");
            printf("El numero correcto era: %d\n",numeroAdivinar);
            return 0;
        }
    }
    printf("GANASTE!! \n");
    return 0;
}

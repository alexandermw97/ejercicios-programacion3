/* Ejercicio 11: meses*/

#include <conio.h>
#include <stdio.h>

main()
{
    int mes;
    printf( "que mes es tu numero?\n\n");
    printf( "Introduzca un numero del 0 al 11: ");
    scanf( "%d", &mes );

    if (mes == 0 )
        {
        printf("Es Enero\n\n" );
        }
    else if (mes == 1 )
        {
        printf("Es Febrero\n\n" );
        }
    else if (mes ==2 )
        {
        printf("Es Marzo\n\n" );
        }
    else if (mes == 3 )
        {
        printf("Es Abril\n\n" );
        }
    else if (mes == 4 )
        {
        printf("Es Mayo\n\n" );
        }
    else if (mes == 5)
        {
        printf("Es Junio\n\n" );
        }
    else if (mes == 6 )
        {
        printf("Es Julio\n\n" );
        }
    else if (mes == 7 )
        {
        printf("Es Agosto\n\n" );
        }
    else if (mes == 8 )
        {
        printf("Es Septiembre\n\n" );
        }
    else if (mes == 9 )
        {
        printf("Es OCtubre\n\n" );
        }
    else if (mes == 10 )
        {
        printf("Es Noviembre\n\n" );
        }
    else if (mes == 11 )
        {
        printf("Es Diciembre\n\n" );
        }
    else
        printf("Dijimos del 0 al 11\n\n");

    getch(); /* Pausa */

    return 0;
}

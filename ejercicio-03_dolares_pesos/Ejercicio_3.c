/* Ejercicio 3: Pesos a dolares a pesos */
#include <stdio.h>

int main ()
{
    float peso, dolar, cambio;

    printf ("Ingrese el valor del dolar:\n $");
    scanf ("%f", & dolar);
    printf ("Escribe la cantidad de dolares a convertir:\n $");
    scanf ("%f", & peso);
    cambio=dolar*peso;
    printf ("La cantidad de dolares es:\n $ %0.3f", cambio);

    return 0;
}

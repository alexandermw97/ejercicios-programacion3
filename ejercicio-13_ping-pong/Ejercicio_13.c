/* Ejercicio 13: Ping-pong*/

#include <stdio.h>
main()
{
    int numero;
    printf("Ingresa un numero: ");
    scanf("%d", &numero);
    if(numero%5==0 && numero%3==0)
    {
        printf("PING PONG!!\n");
    }
   else if(numero%3==0)
    {
        printf("PING !\n");
    }
    else if (numero%5==0)
    {
        printf("PONG !\n");
    }
    else
    {
        printf("No es divisible con 5 ni con 3\n\n");
    }
   return 0;
}

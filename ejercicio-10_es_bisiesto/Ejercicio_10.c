/* Ejercicio 10: a�o bisiesto*/

#include <conio.h>
#include <stdio.h>

main()
{
    int anho;
    printf( "tu anho es bisiesto o no?\n\n");
    printf( "Introduzca una fecha: ");
    scanf( "%d", &anho );

    if ( anho % 4 == 0 && anho % 100 != 0 || anho % 400 == 0 )
        printf("Es bisiesto\n\n" );
    else
        printf("No es bisiesto\n\n");

    getch();
    return 0;
}
